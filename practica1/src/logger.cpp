#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>


int main(int argc, char* argv[]) {
	
	char buff[200];
	int salir=0;
	int aux;
	if(mkfifo("/tmp/loggerfifo",0666)==0){
		return 0;
		perror("mkfifo");
	}

	int fd=open("/tmp/loggerfifo", O_RDONLY);

	if(fd==-1){
		perror("Opening FILE");
		exit(1);
	}
	else{
		while(salir==0){
			aux=read(fd,buff,sizeof(buff));
			printf("- %s\n", buff);
			if(buff[0]=='C'||aux==-1)
		   	{
				printf("Tenis closed. Closing logger... \n");
				salir=1; 
		  	}
            	if(buff[0]=='C')	break;	
		}
	}
	close(fd);
	printf("Closed \n");
	unlink("/tmp/loggerfifo");
	printf("UNLINK\n");
	return 0;

}
