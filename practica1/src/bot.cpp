#pragma once

#include <iostream>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <unistd.h>

#include "DatosMemCompartida.h"
#include "Esfera.h"
#include "Raqueta.h"

int main()

{
	int fdbot;
	DatosMemCompartida* pMemComp;
	char* proyeccion;
	fdbot=open("/tmp/datosBot.txt",O_RDWR);
	proyeccion=(char*)mmap(NULL,sizeof(*(pMemComp)),PROT_WRITE|PROT_READ,MAP_SHARED,fdbot,0);
	close(fdbot);
	pMemComp=(DatosMemCompartida*)proyeccion;

	while(1)
	{
		float posRaqueta;
		posRaqueta=((pMemComp->raqueta1.y2+pMemComp->raqueta1.y1)/2);
		if(posRaqueta<pMemComp->esfera.centro.y)
			pMemComp->accion=1;
		else if(posRaqueta>pMemComp->esfera.centro.y)
			pMemComp->accion=-1;
		else
			pMemComp->accion=0;
	
		//usleep(15000); 
	}

	munmap(proyeccion,sizeof(*(pMemComp)));

}
